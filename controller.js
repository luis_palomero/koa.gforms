
angular
  .controller('gformsController', loadFunction);

loadFunction.$inject = ['$scope', '$http', 'structureService', '$location'];

function loadFunction($scope, $http, structureService, $location){
  structureService.registerModule($location, $scope, "gforms");
}