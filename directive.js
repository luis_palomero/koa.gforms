angular.directive('gForm', [function(){
  return {
    restrict: 'E',
    scope: {
      id: '=',
      width: '=',
      height: '='
    },
    link: function(scope, element, attrs){
      var height = scope.height || "100%";
      var width = scope.width || "100%";

      element.html('<iframe src="https://docs.google.com/forms/d/'+scope.id+'/viewform?embedded=true" width="' + width + '" height="' + height + '" frameborder="0" marginheight="0" marginwidth="0">{{gform.loading|translate}}</iframe>');
    }
  }
}]);